import requests as reqs
import bs4 as bs
import re

final = [];


def getWords(url):

	results = reqs.get(url);

	src = results.content;
	soup = bs.BeautifulSoup(src,'lxml');
	links = (soup.find_all("p"));
	dic = {}
	i = 0;

	for link in links:
	    hits = link.text.strip()
	    dic[i] = hits
	    i+=1

	for obj in dic:
		fl = dic[obj].split(" ")

		for j in fl:
			if not j.isalpha() or len(j) < 5 : 
				fl.remove(j)

		for k in fl:
			if len(k) > 4 and k.isalpha():
				if k not in final:
					final.append(k);
				# if k in final:
				# 	final[k] += 1
				# else:
				# 	final[k] = 1


urls = {
	0 : "https://en.wikipedia.org/wiki/Man",
	1 : "https://en.wikipedia.org/wiki/Woman",
	2 : "https://en.wikipedia.org/wiki/Movie",
	3 : "https://en.wikipedia.org/wiki/Sport",
	4 : "https://en.wikipedia.org/wiki/Video",
	5 : "https://en.wikipedia.org/wiki/Music",
	6 : "https://en.wikipedia.org/wiki/Technology"
}


for i in urls:
	getWords(urls[i]);

print(len(final));

import json
with open('data.json', 'w', encoding='utf-8') as f:
    json.dump(final, f, ensure_ascii=False, indent=4)
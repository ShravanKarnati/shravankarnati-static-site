class Hangman {
    constructor() {
        this.allWords = "https://bitbucket.org/ShravanKarnati/hangman/raw/4401c61ef0d96d8db86d0eec927d5f84cde35f9f/words(scraped)/data.json";
        this.chances = -1;
        this.svg = ['head', 'spine', 'hands', 'legs', "door"];   
        this.word = "";
    }

    getWord = () => {
        fetch(this.allWords)
            .then(res => res.json())
            .then(data => {
                let randNum = Math.ceil(Math.random() * data.length);
                this.word = data[randNum];
                console.log(this.word);
                this.makeWord();
            })
            .catch(err => {
                document.querySelector(".reset").click();
            });
    }

    ///// make dashed without words --> invisible
    makeWord = () => {
        let html = "";
        var regex = /^[a-zA-Z]+$/;
        for(let i = 0; i<this.word.length; i++)
        {
            (this.word[i].match(regex)) ? html += `<div class="letter-div"><p class="letter hide-letter">${this.word[i]}</p></div>` : 
                                          html += `<div class="letter-div"><p class="blank">&nbsp;</p></div>`; 
        }
        document.querySelector(".word").innerHTML = html;

        //// Guessing the word,, adding event listener to guess function
        document.querySelector(".guess").addEventListener("click", this.guessClicked);

    }

    //// guessed event listener function
    guessClicked = () => {
        let inputBox = document.querySelector(".input");
        let wrongBucket = [];
        if (inputBox.value) {
            this.checkLetter(inputBox.value, wrongBucket);
        }
        inputBox.value = "";
        inputBox.focus();
    }

    //// Checking the letter
    checkLetter = (letter, wrongBucket) => {
        if(this.word.includes(letter)) {
            this.correctGuess(letter);
            this.gameWon();
        } else {
            this.wrongGuess(letter, wrongBucket);
        }
    }

    //// If a letter is correctly guessed in the word.
    correctGuess = (letter) => {
        let allLetters = document.querySelectorAll(".letter");
        allLetters.forEach(cur => {
            if (cur.textContent == letter) {
                cur.classList.add("show-letter");
                cur.classList.remove("hide-letter");
            }
        })
    }

    //// If guess is wrong.
    wrongGuess = (letter, wrongBucket) => {
        if (!wrongBucket.includes(letter)) {
            this.svgDraw();
        }
    }

    //// When guessed wrong, construct the svg.
    svgDraw = () => {
        this.chances++;
        let now = this.svg[this.chances];

        switch (now) {
            case "head":
                document.querySelectorAll(".head").forEach(cur => cur.classList.remove("hideSVG"));
                break;
            case "spine":
                document.querySelectorAll(".spine").forEach(cur => cur.classList.remove("hideSVG"));
                break;
            case "hands":
                document.querySelectorAll(".hands").forEach(cur => cur.classList.remove("hideSVG"));
                break;
            case "legs":
                document.querySelectorAll(".legs").forEach(cur => cur.classList.remove("hideSVG"));
                break;
            case "door":
                this.hangmanDied();
                this.gameLost();
                break;
        }
    };

    //// If used all chances and couldn't guess the word ---> hanging the hangman.
    hangmanDied = () => {

        $("#animate").click(function(){
            dropBody();
            $("#rEyes").addClass("hide");
            $("#xEyes").removeClass("hide");
        });

        function dropBody () {
          $("#door1").velocity({rotateZ: 90}, 1000);
          $("#door2").velocity({rotateZ: -90}, 1000);
          fall();  
        }


        function fall() {
          let dur = 500;
          let del = 1000;
          $("#body").velocity({translateY: "200px"}, {duration: dur, delay: del});
          $("#rope").velocity({y2: "+=200px"}, {duration: dur, delay: del});
          $("#armL").velocity({y2: "-=60px"}, {duration: dur, delay: del});
          $("#armR").velocity({y2: "-=60px"}, {duration: dur, delay: del});

          finish();
        }

        function finish () {
          $("#armL").velocity({y2: "+=70px", x2: "+=10px"}, 500);
          $("#armR").velocity({y2: "+=70px", x2: "-=10px"}, 500);
        }

        document.querySelector("#animate").click();

    };

    //// If the game is lost.
    gameLost = () => {
        document.querySelector(".guess").removeEventListener("click",this.guessClicked);
        let hiddenLetters = document.querySelectorAll(".hide-letter");
        hiddenLetters.forEach(cur => {
            cur.classList.remove('hide-letter')
        })
        this.endGame();
    }

    //// If the game is won.
    gameWon = () => {
        let hiddenLetters = document.querySelectorAll(".hide-letter");
        if (hiddenLetters.length === 0) {
            console.log("completed");
            this.endGame();
        }
    }

    //// Ending the game.
    endGame = () =>{
        console.log("game finishhhh!!!");
    }

}


hangMan = new Hangman().getWord();